CREATE DATABASE  IF NOT EXISTS `tedbay` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tedbay`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: 127.0.0.1    Database: tedbay
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auction`
--

DROP TABLE IF EXISTS `auction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction` (
  `id_offerer` varchar(45) NOT NULL,
  `id_product` varchar(20) NOT NULL,
  `numberofbids` int(11) NOT NULL,
  `started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ends` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `currently` decimal(10,2) NOT NULL,
  `buyprice` decimal(10,2) DEFAULT NULL,
  `firstbid` decimal(10,2) NOT NULL,
  `active_a` int(11) NOT NULL,
  `id_bidder` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_offerer`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction`
--

LOCK TABLES `auction` WRITE;
/*!40000 ALTER TABLE `auction` DISABLE KEYS */;
INSERT INTO `auction` VALUES ('seller95','10',3,'2015-09-03 22:11:47','2017-03-01 21:32:02',50.06,90.00,40.00,1,'bidder95'),('seller95','11',1,'2015-09-03 22:11:54','2015-11-30 23:00:01',234.00,500.00,190.00,1,'random95'),('seller95','14',0,'2015-09-03 23:02:38','2017-03-31 22:01:01',45.00,100.00,45.00,1,NULL),('seller95','15',1,'2015-09-03 23:03:46','2017-06-30 22:01:01',5.00,10.00,2.00,1,'bidder95'),('seller95','24',0,'2015-09-13 22:38:38','2016-07-31 22:01:01',10.00,100.00,10.00,0,NULL),('seller95','25',0,'2015-09-14 16:43:25','2016-01-31 23:01:01',1.00,10.00,1.00,2,NULL),('seller95','27',0,'2015-09-14 16:37:21','2016-05-01 07:01:01',10.00,10000.00,10.00,0,NULL),('seller95','28',0,'2015-09-14 16:43:47','2016-01-31 23:01:01',0.01,0.01,0.01,1,NULL),('seller95','52',0,'2015-09-14 20:35:59','2015-08-31 22:01:01',100.00,0.00,100.00,2,NULL),('seller95','53',0,'2015-09-14 20:49:18','2015-09-14 20:51:01',1.00,100.00,1.00,2,NULL),('seller95','54',0,'2015-09-14 20:50:03','2015-09-14 20:51:01',0.10,1.00,0.10,2,NULL),('seller95','55',0,'2015-09-15 00:33:26','2015-12-31 23:01:01',10.00,40.00,10.00,0,NULL),('seller95','67',0,'2015-09-15 03:19:03','2015-09-30 21:01:01',1.00,10.00,1.00,1,NULL),('seller95','69',0,'2015-09-17 04:55:17','2016-01-31 23:01:01',60.00,500.00,60.00,1,NULL),('seller95','8',1,'2015-09-03 22:11:58','2016-04-03 22:01:01',40.00,60.00,10.00,1,'girl95'),('seller95','9',2,'2015-09-03 22:12:01','2016-12-02 01:02:01',22.00,30.00,15.00,1,'girl95');
/*!40000 ALTER TABLE `auction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bid`
--

DROP TABLE IF EXISTS `bid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bid` (
  `id_bidder` varchar(45) NOT NULL,
  `id_product` varchar(20) NOT NULL,
  `bid_value` decimal(10,2) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bid`
--

LOCK TABLES `bid` WRITE;
/*!40000 ALTER TABLE `bid` DISABLE KEYS */;
INSERT INTO `bid` VALUES ('random95','9',18.00,'2015-09-03 22:54:12'),('random95','10',50.00,'2015-09-03 22:54:26'),('random95','11',234.00,'2015-09-03 22:54:43'),('girl95','8',40.00,'2015-09-03 22:55:23'),('girl95','9',22.00,'2015-09-03 22:55:41'),('bidder95','15',5.00,'2015-09-03 23:04:17'),('bidder95','10',50.06,'2015-09-15 02:30:50');
/*!40000 ALTER TABLE `bid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buy`
--

DROP TABLE IF EXISTS `buy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buy` (
  `id_bidder` varchar(45) NOT NULL,
  `id_product` varchar(20) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active_b` int(11) NOT NULL,
  PRIMARY KEY (`id_bidder`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buy`
--

LOCK TABLES `buy` WRITE;
/*!40000 ALTER TABLE `buy` DISABLE KEYS */;
/*!40000 ALTER TABLE `buy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES ('animation'),('cleaning'),('food'),('music'),('nerds');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_has_product`
--

DROP TABLE IF EXISTS `category_has_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_has_product` (
  `category_name` varchar(45) NOT NULL,
  `product_id` varchar(20) NOT NULL,
  PRIMARY KEY (`category_name`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_has_product`
--

LOCK TABLES `category_has_product` WRITE;
/*!40000 ALTER TABLE `category_has_product` DISABLE KEYS */;
INSERT INTO `category_has_product` VALUES ('animation','14'),('animation','2'),('animation','24'),('cleaning','25'),('cleaning','27'),('cleaning','28'),('food','55'),('food','67'),('music','69'),('nerds','14');
/*!40000 ALTER TABLE `category_has_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_has_user`
--

DROP TABLE IF EXISTS `category_has_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_has_user` (
  `category_name` varchar(50) NOT NULL,
  `user_id` varchar(45) NOT NULL,
  PRIMARY KEY (`category_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_has_user`
--

LOCK TABLES `category_has_user` WRITE;
/*!40000 ALTER TABLE `category_has_user` DISABLE KEYS */;
INSERT INTO `category_has_user` VALUES ('food','bidder95'),('music','bidder95'),('nerds','bidder94'),('nerds','bidder95'),('nerds','girl95');
/*!40000 ALTER TABLE `category_has_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (''),('APO'),('Argentina'),('Australia'),('Belgium'),('Brasil'),('Bulgaria'),('Canada'),('China'),('Croatia, Republic of'),('France'),('Germany'),('Greece'),('Iran'),('Italy'),('Japan'),('Latvia'),('Malta'),('Netherlands'),('New Zealand'),('Russian Federation'),('Singapore'),('Spain'),('UK'),('United Kingdom'),('USA');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reciever` varchar(45) DEFAULT NULL,
  `id_sender` varchar(45) DEFAULT NULL,
  `text` text NOT NULL,
  `date` varchar(25) NOT NULL,
  `read` tinyint(1) NOT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `deleted_r` tinyint(4) DEFAULT '0',
  `deleted_s` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'bidder94','bidder94','Hey big bro','Sep-04-15 02:05:08',0,'Hello',1,1),(2,'Bidder95','bidder94','How are you doing my rasta?','Sep-04-15 02:06:27',1,'hey bro',1,1),(3,'bidder94','bidder95','good brethen and you?','Sep-04-15 02:06:56',1,'RE:hey',1,1),(4,'bidder95','bidder95','yo','Sep-04-15 03:09:04',0,'lol',0,0),(5,'bidder95','bidder95','jkjk','Sep-04-15 03:09:14',0,'RE:lol',0,0),(6,'bidder95','bidder95','kjkjk','Sep-04-15 03:09:22',0,'RE:lol',0,0),(7,'bidder95','bidder95','jkjkj','Sep-04-15 03:09:28',0,'RE:RE:lol',1,0),(8,'bidder95','bidder95','jkjkj','Sep-04-15 03:09:46',0,'RE:RE:RE:lol',0,0),(9,'bidder95','bidder95','jkjkjk','Sep-04-15 03:09:55',0,'RE:RE:RE:RE:lol',1,0),(10,'bidder95','bidder95','ghghghg','Sep-04-15 03:14:25',0,'RE:RE:RE:RE:lol',0,0),(11,'bidder95','bidder95','ewew','Sep-04-15 03:14:50',0,'wewe',1,0),(12,'bidder95','bidder95','sdsds','Sep-04-15 03:15:44',0,'RE:lol',0,0),(13,'seller95','seller95','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo','Sep-12-15 16:19:05',0,'12314',0,0),(14,'bidder95','bidder95','dsds\'dsdsd \'ds dsd\' ','Sep-13-15 21:02:49',0,'test?',0,0),(15,'bidder95','bidder95','sdskdjskjdsdsjkdjskdjksdjskjdksdjksjdksjdksjdkjskds;dsdsdsdsdsdsdsdsdsdsdsds','Sep-13-15 21:03:23',0,'1234',0,0),(16,'bidder95','bidder95','asjdksajdksajjfhg askdjfg adskfga d sajkdjak dd sajdkasjd asjkdjsakjdkj jkdsjadk jksajdk jdhsajdh jdsadkjk jdksjakd jkdsjad jk djsakdj sajdks jskadj sajdkjsa djksajdksa djkasjd sajkdjsak djaskd jasjdhjgjhsdga sdggsfhdskghfg dsfghasjkfhg adsjhf gasdfgdjsa fgasd jkfgsadjk fgjahsd fgjdsahg fjhagf jdahgsf jkahsdg fjkasgdf jkashg fkjashgdf kjashgfjka sdhgf kasjdhfg aksjdfg aksjfgh aksdjfg asdjkfgh aksdjhfg aksd','Sep-13-15 21:30:19',0,'123',0,0),(17,'bidder95','seller95','spgeti mych?','Sep-14-15 23:32:19',0,'spaggeti',0,0),(18,'seller95','bidder95','yes pl0x','Sep-14-15 23:32:45',0,'RE:spaggeti',0,0),(19,'bidder95','seller95','will do soon mohn','Sep-15-15 00:00:21',0,'RE:RE:spaggeti',0,0),(20,'crocodillon','crocodillon','dsdsdsds','Sep-15-15 05:09:13',1,'1234',1,1),(21,'crocodillon','crocodillon','dsdsds','Sep-15-15 05:09:46',1,'RE:1234',1,1),(22,'crocodillon','crocodillon','dsds','Sep-15-15 05:10:33',0,'wewd',1,1),(23,'crocodillon','crocodillon','sdsdsds','Sep-15-15 05:11:32',1,'1234',1,1),(24,'crocodillon','crocodillon','dsdsd','Sep-15-15 05:11:39',1,'RE:1234',1,1),(25,'crocodillon','crocodillon','dsds','Sep-15-15 05:12:11',1,'sdsds',1,1),(26,'crocodillon','crocodillon','','Sep-15-15 05:20:41',1,'12345',1,1),(27,'crocodillon','crocodillon','','Sep-15-15 05:29:30',1,'1234',1,1),(28,'crocodillon','crocodillon','sdsds\r\n','Sep-15-15 05:29:40',1,'1234',1,1);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `location` varchar(150) DEFAULT NULL,
  `lon` varchar(45) DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `country` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `active_p` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  FULLTEXT KEY `name` (`name`,`description`)
) ENGINE=InnoDB AUTO_INCREMENT=1308816986 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (2,'Spiderman #1','New York','12','12','Spain','Spoderman!','http://localhost:8888/images/2',0),(5,'temp','tete','12','12','UK','temp prod','http://localhost:8888/images/5',0),(8,'Hair Dryer','gurls land','12','8','Spain','Super hair dryer makes you pretty !','http://localhost:8888/images/8',1),(9,'Cat Food','lonely planet','9','8','France','Best catfood makes your cat fly!','http://localhost:8888/images/9',1),(10,'Moonshine','Moon','12','13.000','USA','Makes you blind hic!','http://localhost:8888/images/10',1),(11,'God Sized Bed','Bedroom','12','14','Spain','best bed for all your harem !','http://localhost:8888/images/11',1),(14,'DareDevil #57','DevilLand','12','13','other','dghajg dgashjgd sajgd asjghdasj gd jasgd jasgdjasg djasgd jasgd jagsd jsagd jasgd jsaghd jashgd jasg djasgd jasgd jsagd jasgd jasgd jasgd jasgd jasgd jsagd jasgd jasgd jasg djasgdjasg djasgdjasg djasg djasg djasgdjgas jdgasjdg asjgdjasgdjasgdja gdjasgd jasg djasgd jasgdjhasg djasg djagsdagsdjags dags jdagsjd ga jsdga jdg jasg djasg djasgdjgas jdgsa jdgasjgdjas gdjsagd ajsgd ajsdgajsdsajdgasjgd ajsgd jasgd jagsdjasg djags dajsga djas dgjsa','http://localhost:8888/images/14',1),(15,'Belgian Gods Beer','Brussels','12','12','Belgium','Best beer makes you trip around the world!','http://localhost:8888/images/15',1),(24,'testing prod','test','12','12','France','test','http://localhost:8888/images//24',1),(25,'Swiffer','Greece','19','12','Greece','Cleanup','http://localhost:8888/images//25',1),(26,'dummy','sds','28','51','USA','dsds','http://localhost:8888/images//26',0),(27,'dummy','sds','28','51','USA','dsds','http://localhost:8888/images//27',0),(28,'dummy','dsds','18','17','USA','dsds','http://localhost:8888/images//28',1),(31,'Barilla Spaggeti no 6','milan','45.450588',' 9.146182','Italy','spag','http://localhost:8888/images//31',0),(52,'na liksei','sds','','','other','','http://localhost:8888/images//52',2),(53,'Î´Ï?Î´','','','','other','','http://localhost:8888/images//53',2),(54,'test2','','','','other','','http://localhost:8888/images//54',2),(55,'Brasilian Coffee','Sao Paolo','-51.32812499999977','-12.554563528593224','Brasil','Best coffe makes you work 30 hour per day 9 days per week at 110%.','http://localhost:8888/images//55',0),(67,'Barilla Spaggeti no 6','Milan','10.95886230468753','44.86754965944777','Italy','Best of the west','http://localhost:8888/images//67',1),(69,'Curius Corn','Hull','-2.109375000000045','52.482780222078226','UK','Best of Ozrics','http://localhost:8888/images//69',1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signup`
--

DROP TABLE IF EXISTS `signup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signup` (
  `username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `tel` varchar(45) NOT NULL,
  `address` varchar(50) NOT NULL,
  `lon` varchar(45) DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `afm` varchar(30) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signup`
--

LOCK TABLES `signup` WRITE;
/*!40000 ALTER TABLE `signup` DISABLE KEYS */;
INSERT INTO `signup` VALUES ('dsds','seller','','','','dsds@ds.fd','','','','','',0);
/*!40000 ALTER TABLE `signup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `lon` varchar(45) DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `afm` varchar(30) DEFAULT NULL,
  `rating` int(11) DEFAULT '0',
  `rates` int(11) DEFAULT '1',
  `seller_rating` int(11) DEFAULT '0',
  `seller_rates` int(11) DEFAULT '1',
  PRIMARY KEY (`username`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('admin95','admin','1234','Adam','Minger','adm@yahoo.gr','210000990','Massachusets','12','11','111529292929',1,1,0,1),('bidder94','bidder','1234','John','Snow','js@gmail.com','21048484848','Wall','12','12','111494949494',2,1,0,1),('bidder95','bidder','1234','Billy','Dermod','bd@yahoo.com','2105555555','Murica','12','13','1115100000234',13,6,0,1),('crocodillon','admin','1234','Sean','Dillon','sdi1000213@di.uoa.gr','210 6666 666','vinegardens','12','13','1115201000213',4,1,0,1),('girl95','bidder','1234','Gianna','Rowling','gr@gmail.com','210000009','Italia','43','14','',40,10,0,1),('random95','bidder','1234','Randalf','Omertopulos','ro@gmail.com','210003283','Georgia','15','19','111547474743',4,1,0,1),('selder95','bidder_seller','1234','Sheldon','Elder','sd@yahoo.gr','2106555555','New York','14','11','111520100056',3,1,0,1),('seller95','seller','1234','Selina','Leproche ','sl@yahoo.com','2105555555','USA','13','12','1115210000032',0,1,15,5);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'tedbay'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `expiration_event` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8 */ ;;
/*!50003 SET character_set_results = utf8 */ ;;
/*!50003 SET collation_connection  = utf8_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `expiration_event` ON SCHEDULE EVERY 1 MINUTE STARTS '2015-09-13 17:39:28' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE `tedbay`.`auction`
SET
`active` = '2'
WHERE NOW() >`auction`.`ends` */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'tedbay'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-17 18:57:06
